<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <base href="/media/">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Armen Kirakosyan">
    <meta name="description" content="Sport news, videos, live results">
    <meta name="keywords" content="sport, news, transfers, football, basketball, soccer, hockey, ufc, mma, live, tables">
    <title>Log in to GOYASPORT</title>
    <style>
        .p {
            color: black;
            font-family: sans-serif,fantasy;
        }
        p {
            color: black;
            font-family: sans-serif,fantasy;
        }
        .a {
            font-family: sans-serif,fantasy;
            text-decoration: none;
            color: #00d693;
        }
        .navbutton {
            font-family: sans-serif,fantasy;
            background-color: #00ba73;
            border-style: none;
            color: #034b20;
            text-decoration: none;
            padding: 15px 25px;
            text-align: center;
            font-size: 16px;
            cursor: pointer;
        }

        .navbutton:hover {
            font-family: sans-serif,fantasy;
            color: #e1dfce;
            background: #034b20;
        }
    </style>
</head>
<body>
<header style="width: auto; height: 100px;background-color: #00c57c;">
    <a style="" href="/index.html"><img src="images/logo.png" width="322" height="72" alt="GOYASPORT_logo"></a>
    <div style="float: right;height: 100px;width: 300px;text-align: center">
        <br><a class="button" href="/pages/login.html">Log in</a>&nbsp;
        <a class="button" href="/pages/register.html">Registration</a>
    </div>
</header>
<nav style="min-height: 50px;background: #00ba73; text-align: center">
    <br><a class="navbutton" href="/pages/login.html">FOOTBALL</a>
    <a class="navbutton" href="/index.html">BASKETBALL</a>
    <a class="navbutton" href="/index.html">TENNIS</a>
    <a class="navbutton" href="/index.html">HOCKEY</a>
    <a class="navbutton" href="/index.html">BASEBALL</a>
    <a class="navbutton" href="/index.html">MMA</a>
    <a class="navbutton" href="/index.html">MORE</a>
</nav>
<aside id="leftbar" style="min-width: 350px;min-height: 1000px;background: #034b20;float: left;">
    <br><ul style="margin: 0 auto">
    <li><a class="a" href="/index.html">This is link, okay?</a></li>
    <li><a class="a" href="/index.html">This is link, okay?</a></li>
    <li><a class="a" href="/index.html">This is link, okay?</a></li>
    <li><a class="a" href="/index.html">This is link, okay?</a></li>
    <li><a class="a" href="/index.html">This is link, okay?</a></li>
    <li><a class="a" href="/index.html">This is link, okay?</a></li>
</ul>
</aside>
<aside style="background: #034b20;float: right;min-width: 350px;min-height: 1000px">
    <p>Aside text right bar</p>
</aside>
<section style="background-color: #9bffd4;min-height: 1000px;min-width: 500px;text-align: center"><br>
    <div style="width: 500px;margin: 0 auto;">
        <fieldset style="background: #00c57c;height: 250px;">
            <legend class="p" style="color: #034b20;font-size: 35px;font-weight: bold;background-color: #9bffd4"> &nbsp; Log In Success&nbsp; </legend>
            <br><span>Email: <?php echo $_GET['username'] ?></span>
            <br><span>Age: <?php echo $_GET['age'] ?></span>
            <br><span>Full name: <?php echo $_GET['fullname'] ?></span>
            <br><span>Gender: <?php echo $_GET['gender'] ?></span>
            <br><span>Eye color: <span style=color:white;background-color:<?php echo $_GET['eyecol']?>> &nbsp<b><?php echo $_GET['eyecol']?></b>&nbsp </span></span>
         </fieldset>
    </div>
</section>
<footer style="background: linear-gradient(to bottom, rgb(165,165,165), rgb(80,80,80));min-height: 80px;color: #000000;text-align: center">
    <br><a class="a" href="http://www.goya.am" style="color: black" target="_blank"><h3>GOYA LLC &copy; 2020</h3></a>
</footer>
</body>
</html>