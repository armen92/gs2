
let section = document.getElementById("section");

function article(art) {
  section.innerHTML = "";
  let h1 = document.createElement("h1");
  h1.append(art);
  section.append(h1);
}

async function getJsonData(url) {
  let response = await fetch(url);
  let data = await response.json();
  return data;
}

function newDiv(i) {
  let newDiv = document.createElement("div");
  if (++i % 2 == 0) {
    newDiv.className = "content-box";
  } else {
    newDiv.className = "content-box-dark";
  }
  section.append(newDiv);
}

function newP(content) {
  let p = document.createElement("p");
  p.append(content);
  section.lastElementChild.append(p);
}

function newH3(content) {
  let h3 = document.createElement("h3");
  h3.append(content);
  section.lastElementChild.append(h3);
}

function newUl(iD, clsName) {
  let ul = document.createElement("ul");
  ul.id = iD;
  ul.className = clsName;
  section.lastElementChild.append(ul);
}

function newLi(content, ulId) {
  let li = document.createElement('li');
  li.innerHTML = content;
  document.getElementById(ulId).append(li);
}

function newImg(status) {
  let img = document.createElement("img");
  if (status) {
    img.src = "/media/images/true.png"
    img.className = "true-false";
    img.alt = 'true';
  } else {
    img.src = "/media/images/false.png"
    img.className = "true-false";
    img.alt = 'false';
  }
  section.lastElementChild.append(img);
}

async function postsHandler() {
  article('POSTS');
  let posts = await getJsonData('https://jsonplaceholder.typicode.com/posts/');
  for (let i = 0; i < posts.length; i++) {
    newDiv(i);
    newP(`#${posts[i].id} UserID: ${posts[i].userId}`);
    newH3(posts[i].title);
    newP(posts[i].body);
  }
}

document.getElementById("posts").addEventListener("click", postsHandler);

async function commentsHandler() {
  article('COMMENTS');
  let comments = await getJsonData('https://jsonplaceholder.typicode.com/comments');
  for (let i = 0; i < comments.length; i++) {
    newDiv(i);
    newP(`#${comments[i].id} UserID: ${comments[i].postId}`);
    newP(`Email: ${comments[i].email}`);
    newH3(comments[i].name);
    newP(comments[i].body);
  }
}

document.getElementById("comments").addEventListener("click", commentsHandler);

async function albumsHandler() {
  article('ALBUMS');
  let albums = await getJsonData('https://jsonplaceholder.typicode.com/albums');
  for (let i = 0; i < albums.length; i++) {
    newDiv(i);
    newP(`#${albums[i].id} UserID: ${albums[i].userId}`);
    newH3(albums[i].title);
  }
}

document.getElementById("albums").addEventListener("click", albumsHandler);

async function todosHandler() {
  article('TODOS');
  let todos = await getJsonData('https://jsonplaceholder.typicode.com/todos');
  for (let i = 0; i < todos.length; i++) {
    newDiv(i);
    newP(`#${todos[i].id} UserID: ${todos[i].userId}`);
    newH3(todos[i].title);
    newImg(todos[i].completed);
  }
}

document.getElementById("todos").addEventListener("click", todosHandler);

async function usersHandler() {
  article('USERS');
  let users = await getJsonData('https://jsonplaceholder.typicode.com/users');
  for (let i = 0; i < users.length; i++) {
    newDiv(i);
    newP(`#${users[i].id}`);
    newUl(`ulUsers${i}`, 'aboutUser');

    newLi(`<b>Full name:</b> ${users[i].name}`, `ulUsers${i}`);
    newLi(`<b>Username:</b> ${users[i].username}`, `ulUsers${i}`);
    newLi(`<b>Email:</b> ${users[i].email}`, `ulUsers${i}`);
    newLi(`<b>Phone:</b> ${users[i].phone}`, `ulUsers${i}`);
    newLi(`&nbsp;`, `ulUsers${i}`);

    newUl(`ulUsers-${i}`, 'aboutUser');
    newLi('<b>Address</b>', `ulUsers-${i}`);
    newLi(`<b>City:</b> ${users[i].address.city}`, `ulUsers-${i}`);
    newLi(`<b>Street:</b> ${users[i].address.street}`, `ulUsers-${i}`);
    newLi(`<b>Suite:</b> ${users[i].address.suite}`, `ulUsers-${i}`);
    newLi(`<b>ZIP-code:</b> ${users[i].address.zipcode}`, `ulUsers-${i}`);

    newUl(`ulUsers--${i}`, 'aboutUser');
    newLi('<b>Company</b>', `ulUsers--${i}`);
    newLi(`<b>Company name:</b> ${users[i].company.name}`, `ulUsers--${i}`);
    newLi(`<b>Catchphrase:</b> ${users[i].company.catchPhrase}`, `ulUsers--${i}`);
    newLi(`<b>Buisness sphere:</b> ${users[i].company.bs}`, `ulUsers--${i}`);
  }
}
  document.getElementById("users").addEventListener("click", usersHandler);